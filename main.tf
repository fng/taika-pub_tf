variable "hcloud_token" {
  sensitive = true # Requires terraform >= 0.14
}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

# Obtain ssh key data
data "hcloud_ssh_key" "ssh_key" {
  fingerprint = "da:08:69:b1:d2:79:66:fc:12:1d:56:98:35:21:fc:76"
}

resource "hcloud_network" "network-pub" {
  name     = "network-pub"
  ip_range = "10.0.0.0/16"
}

resource "hcloud_primary_ip" "k3s-server-pub-ip" {
  name          = "primary_ip_server"
  datacenter    = "nbg1-dc3"
  type          = "ipv4"
  assignee_type = "server"
  auto_delete   = false
  delete_protection = false
  labels = {
    "k3s" : "server"
  }
}

resource "hcloud_network_subnet" "k3s-subnet" {
  type         = "cloud"
  network_id   = hcloud_network.network-pub.id
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/24"
}

resource "hcloud_server" "k3s-01-server" {
  name        = "k3s-01-server"
  server_type = "cpx11"
  image       = "debian-11"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
  location    = "nbg1"
  user_data = file("user-data-server.yml")

  network {
    network_id = hcloud_network.network-pub.id
    ip         = "10.0.1.1"
  }
  public_net {
    ipv4_enabled = true
    ipv4 = hcloud_primary_ip.k3s-server-pub-ip.id
    ipv6_enabled = false
  }
  depends_on = [
    hcloud_network_subnet.k3s-subnet
  ]
}

resource "hcloud_server" "k3s-02-agent" {
  name        = "k3s-02-agent"
  server_type = "cpx11"
  image       = "debian-11"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
  location    = "nbg1"

  network {
    network_id = hcloud_network.network-pub.id
    ip         = "10.0.1.2"
  }
  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }
  depends_on = [
    hcloud_network_subnet.k3s-subnet,
    hcloud_server.k3s-01-server
  ]
}

resource "hcloud_server" "k3s-03-agent" {
  name        = "k3s-03-agent"
  server_type = "cpx11"
  image       = "debian-11"
  ssh_keys    = ["${data.hcloud_ssh_key.ssh_key.id}"]
  location    = "nbg1"

  network {
    network_id = hcloud_network.network-pub.id
    ip         = "10.0.1.3"
  }
  public_net {
    ipv4_enabled = false
    ipv6_enabled = false
  }
  depends_on = [
    hcloud_network_subnet.k3s-subnet,
    hcloud_server.k3s-01-server
  ]
}